import axios from 'axios'; 
import { useEffect, useState } from 'react';
import { Button, Form } from 'react-bootstrap';
import { Link, useNavigate } from 'react-router-dom';
import './createWeb.scss';

function CreateWeb() {

  const navigate = useNavigate();
  const [elements, setElements] = useState([]);
  useEffect(() => {
    const baseURL = "http://localhost:3001/createWeb/getElements/";
    axios.get(baseURL).then((response) => {
      if (response.data.error) {
        alert(response.data.error);
      } else {
        setElements(response.data);
      }
    });
  }, []);

  const remove = (rawPosition) => {
    const divElement = document.getElementById(rawPosition+"div");
    divElement.innerHTML = `
      <form class="">
        <div class="mb-3">
          <label class="form-label" for="${rawPosition}Select">Element</label>
          <select class="form-select" id="${rawPosition}Select">
            ${elements.map((element) => {
              return `<option value="${element.confg}">${element.name}</option>`
            })}
          </select>
        </div>
      </form>
      <button id="${rawPosition}add" type="button" class="btn btn-primary">Select element</button>
    `;
    document.getElementById(rawPosition+"add").onclick = () => {
      selectElement(rawPosition);
    }

  };

  const selectElement = (rawPosition) => {
    const divElement = document.getElementById(rawPosition+"div");
    const elementType = document.getElementById(rawPosition+"Select").value;
    const confg = elementType.split("-");
    const obj = document.createElement(confg[1]);
    obj.id = rawPosition+"obj";
    obj.name = confg[0];
    obj.classList.add("form-control");
    divElement.innerHTML = `
    <div class='text-right p-2'>
      <Button id='${rawPosition}remove' type="button" class="btn btn-primary">
        <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-trash3-fill" viewBox="0 0 16 16">
          <path d="M11 1.5v1h3.5a.5.5 0 0 1 0 1h-.538l-.853 10.66A2 2 0 0 1 11.115 16h-6.23a2 2 0 0 1-1.994-1.84L2.038 3.5H1.5a.5.5 0 0 1 0-1H5v-1A1.5 1.5 0 0 1 6.5 0h3A1.5 1.5 0 0 1 11 1.5Zm-5 0v1h4v-1a.5.5 0 0 0-.5-.5h-3a.5.5 0 0 0-.5.5ZM4.5 5.029l.5 8.5a.5.5 0 1 0 .998-.06l-.5-8.5a.5.5 0 1 0-.998.06Zm6.53-.528a.5.5 0 0 0-.528.47l-.5 8.5a.5.5 0 0 0 .998.058l.5-8.5a.5.5 0 0 0-.47-.528ZM8 4.5a.5.5 0 0 0-.5.5v8.5a.5.5 0 0 0 1 0V5a.5.5 0 0 0-.5-.5Z"/>
        </svg>
      </Button>
    </div>
    <h4>${confg[0]}</h4>
    `;
    document.getElementById(rawPosition+"remove").onclick = () => {
      remove(rawPosition);
    };
    divElement.appendChild(obj)
  };
  const saveWebPage = () => {
    let jsonArray = [];
    for (let indexA = 1; indexA <= 3; indexA++) {
      for (let indexB = 1; indexB <= 3; indexB++) {
        if(!document.getElementById(indexA+""+indexB+"Select")){
          const campo = document.getElementById(indexA+""+indexB+"obj");
          jsonArray.push({
            section:indexA+""+indexB,
            html:campo.name,
            text:campo.value
          })
        }
      }
    }
    const baseURL = "http://localhost:3001/createWeb/createWeb/";
    axios.post(baseURL,{
      user: JSON.parse(localStorage.getItem("userData")).user_name,
      pageName: "empty",
      section: JSON.stringify(jsonArray)
    }).then((response) => {
      if (response.data.error) {
        alert(response.data.error);
      } else {
        alert("Web page created successful");
        navigate("/home");
      }
    });

  };
  const formElement = (rawPosition) => <>
    <Form>
      <Form.Group className="mb-3">
        <Form.Label htmlFor={rawPosition+"Select"}>Element</Form.Label>
        <Form.Select id={rawPosition+"Select"}>
          {
            elements.map((element) => {
              return <option value={element.confg}>{element.name}</option>
            })
          }
        </Form.Select>
      </Form.Group>
    </Form>
    <Button variant='primary' onClick={() => selectElement(rawPosition)}>Select element</Button>
  </>;

  return (
    <div className="createWeb">
      <div className='text-right pt-3'>
        <i variant="primary">
          <Link to={"/home"}>Back</Link>
        </i>
      </div>
      <div className='row pb-5'>
        <div id='11div' className='elementBlock col-4 pt-2'>
          {formElement(11)}
        </div>
        <div id='12div' className='elementBlock col-4 pt-2'>
          {formElement(12)}
        </div>
        <div id='13div' className='elementBlock col-4 pt-2'>
          {formElement(13)}
        </div>
        <div id='21div' className='elementBlock col-4 pt-2'>
          {formElement(21)}
        </div>
        <div id='22div' className='elementBlock col-4 pt-2'>
          {formElement(22)}
        </div>
        <div id='23div' className='elementBlock col-4 pt-2'>
          {formElement(23)}
        </div>
        <div id='31div' className='elementBlock col-4 pt-2'>
          {formElement(31)}
        </div>
        <div id='32div' className='elementBlock col-4 pt-2'>
          {formElement(32)}
        </div>
        <div id='33div' className='elementBlock col-4 pt-2'>
          {formElement(33)}
        </div>
        <div className='col-12 pt-2 text-right'>
          <Button variant="primary" onClick={() => saveWebPage()}>Create</Button>
        </div>
      </div>
    </div>
  );
}

export default CreateWeb;

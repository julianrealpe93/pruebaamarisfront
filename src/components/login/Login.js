import { useState } from 'react';
import { Button, Card, Form } from 'react-bootstrap';
import { Link, useNavigate } from "react-router-dom";
import axios from "axios";
import './Login.scss';

function Login() {

  const navigate = useNavigate();
  const [userName, setUserName] = useState("");
  const [userPassword, setUserPassword] = useState("");

  const loginAccion = () => {
    try {
      if(!userName || !userPassword){
        throw new Error();
      }else{

        const baseURL = "http://localhost:3001/login/";

        axios.post(baseURL,{
          user:userName,
          password:userPassword
        }).then((response) => {
          if(response.data.error){
            alert(response.data.error);
          }else{
            localStorage.setItem("userData", JSON.stringify(response.data.userData));
            localStorage.setItem("token", response.data.token);
            navigate("/home");
          }
        });
      }
    } catch (error) {
      alert("You must fill in all the fields");
    }
  };

  return (
    <div className="LoginMain">
      <Card className="text-center">
        <Card.Header>
          <h3>Login</h3>
          </Card.Header>
        <Card.Body>
          <Form>
            <Form.Group>
              <Form.Label>User</Form.Label>
              <Form.Control placeholder='User name' value={userName} onChange={e => setUserName(e.target.value)}></Form.Control>
              <hr/>
              <Form.Label>Password</Form.Label>
              <Form.Control placeholder='User password' type="password" value={userPassword} onChange={e => setUserPassword(e.target.value)}></Form.Control>
            </Form.Group>
          </Form>
        </Card.Body>
        <Card.Footer>
          <Button variant="primary" onClick={()=>loginAccion()}>Log in</Button>
          <br/>
          <i variant="primary">
            <Link to={"/createUser"}>Create user</Link>
          </i>
        </Card.Footer>
      </Card>
    </div>
  );
}

export default Login;
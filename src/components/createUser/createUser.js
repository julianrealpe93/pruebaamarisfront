import { useEffect, useState } from 'react';
import { Button, Card, Form } from 'react-bootstrap';
import { Link } from "react-router-dom";
import axios from "axios";
import './createUser.scss';

function CreateUser() {

  const [userName, setUserName] = useState("");
  const [userPassword, setUserPassword] = useState("");
  const [role, setRole] = useState(null);
  const [roles, setRoles] = useState([]);

  useEffect(() => {
    const baseURL = "http://localhost:3001/createUser/roles/";
    axios.get(baseURL).then((response) => {
      if (response.data.error) {
        alert(response.data.error);
      } else {
        setRoles(response.data);
        if(response.data.length){
          setRole(response.data[0].id);
        }
      }
    });
  }, []);

  const createAccion = () => {
    try {
      if (!userName || !userPassword || !role) {
        throw new Error();
      } else {
        const URLcreateUser = "http://localhost:3001/createUser/createUser/";
        axios.post(URLcreateUser,{
          user: userName,
          password: userPassword,
          role: parseInt(role)
        }).then((response) => {
          if (response.data.error) {
            alert(response.data.error);
          } else {
            alert("Create user successful")
          }
        });
      }
    } catch (error) {
      alert("You must fill in all the fields");
    }
  };

  return (
    <div className="LoginMain">
      <Card className="text-center">
        <Card.Header>
          <h3>Create user</h3>
        </Card.Header>
        <Card.Body>
          <Form>
            <Form.Group>
              <Form.Label>User</Form.Label>
              <Form.Control placeholder='User name' value={userName} onChange={e => setUserName(e.target.value)}></Form.Control>
              <hr />
              <Form.Label>Password</Form.Label>
              <Form.Control placeholder='User password' type="password" value={userPassword} onChange={e => setUserPassword(e.target.value)}></Form.Control>
              <hr />
              <Form.Group className="mb-3">
                <Form.Label htmlFor="disabledSelect">Role</Form.Label>
                <Form.Select id="disabledSelect" onChange={e => setRole(e.target.value)}>
                  {
                    roles.map((element) => {
                      return <option value={element.id}>{element.name}</option>
                    })
                  }
                </Form.Select>
              </Form.Group>
            </Form.Group>
          </Form>
        </Card.Body>
        <Card.Footer>
          <Link to={"/"} style={{ paddingRight: 10 }}>
            <Button variant="primary">Back</Button>
          </Link>
          <Button variant="primary" onClick={() => createAccion()}>Create</Button>
        </Card.Footer>
      </Card>
    </div>
  );
}

export default CreateUser;
import React from 'react';
import ReactDOM from 'react-dom/client';
import './index.scss';
import Login from './components/login/Login';
import CreateUser from './components/createUser/createUser';
import Home from './components/homepage/Home';
import CreateWeb from './components/createWeb/createWeb';
import reportWebVitals from './reportWebVitals';
import { createBrowserRouter, RouterProvider } from "react-router-dom";

const router = createBrowserRouter([
  {path: "/", element: <Login />, errorElement: <Login />},
  {path: "/createUser", element: <CreateUser />},
  {path: "/home", element: <Home />},
  {path: "/createWeb", element: <CreateWeb />},
]);

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <React.StrictMode>
    <div className='main'>
      <RouterProvider router={router} />
    </div>
  </React.StrictMode>
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
